Feature: Page Loading
  As a user,
  I want to load the page
  So that the dazoodling can be splookified.

  @DEMO-1 @wip
  Scenario: Simple page loading
    
    Given I type webpage address in browser
    When I load the page
    Then I should see a greeting

  @DEMO-2 @foo
  Scenario: Not so simple page loading
    
    Given I type incorrect webpage address in browser
    When I load the page
    Then I should not see a greeting